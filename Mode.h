/* Datei funktionen.h */

#ifndef MODE_CLASS_H
#define MODE_CLASS_H

#include <AccelStepper.h>
#include "Arduino.h"

#define steps_Revolution 200.0
#define revolution_to_top 43.0
#define revolution_to_360 18.35 //18.35
#define limit_switch 4
#define limit_switch_rotation 1
#define shutter 0



class Mode {
  public:

    //Konstruktoren für die Klasse Modi

    Mode();
    //      Schritte Arm, Schritte Rotation der Platte, Erster Stepper, Zweiter Stepper
    Mode(int s_height , int s_rotation , AccelStepper Stepper_1 , AccelStepper Stepper_2 );
    //Name des modis, Schritte Arm, Schritte Rotation der Platte, Starthöhe in Umderehungen, Endhöhe in Umdrehungen, Erster Stepper, Zweiter Stepper
    Mode(String Name , int s_height , int s_rotation , int st_height , int e_height , AccelStepper Stepper_1 , AccelStepper Stepper_2 );
    Mode(const Mode& a);// copy konstruktor
    ~Mode();//destruktor

    void setMode(int s_height , int s_rotation , int st_height , int e_height );
    void setMode(int s__height , int s__rotation );

    void run_mode();

    int get_current_rotation();
    int get_current_hight();
    int get_starting_height();
    int get_ending_height();
    String get_Name();

  private:

    String Name;
    int steps_height;
    int steps_rotation;

    AccelStepper Stepper1;
    AccelStepper Stepper2;

    int current_rotation = 0;
    int current_hight = 0;
    int starting_height = 0;
    int ending_height = revolution_to_top;



};

#endif
