#include "Mode.h"

Mode::Mode():steps_height(4), starting_height(2), ending_height(2), steps_rotation(36),Stepper1(AccelStepper(200, 6, 7, 8, 9)), Stepper2(AccelStepper(200, 10, 11, 12, 13)){
  
}

Mode::Mode(int s_height, int s_rotation, AccelStepper Stepper_1, AccelStepper Stepper_2): steps_height(s_height), steps_rotation(s_rotation), Stepper1(Stepper_1), Stepper2(Stepper_2) {
  
}

Mode::Mode(String Name,int s_height, int s_rotation, int st_height, int e_height, AccelStepper Stepper_1, AccelStepper Stepper_2): steps_height(s_height), steps_rotation(s_rotation), Stepper1(Stepper_1), Stepper2(Stepper_2) {
  this->Name = Name;
  if (st_height <= revolution_to_top) {
    this->starting_height = st_height;
  }
  if (e_height <= revolution_to_top && e_height >= st_height) {
    this->ending_height = e_height;
  }
 
}

Mode::Mode(const Mode& a):steps_height(a.steps_height),steps_rotation(a.steps_rotation),starting_height(a.starting_height),ending_height(a.ending_height),Stepper1(a.Stepper1), Stepper2(a.Stepper2){
  
}


Mode::~Mode() {
}

void Mode::setMode(int s_height, int s_rotation, int st_height, int e_height) {
  this->steps_height = s_height;
  this->steps_rotation = s_rotation;

  if (st_height <= revolution_to_top) {
    this->starting_height = st_height;
  }
  if (e_height <= revolution_to_top && e_height >= st_height) {

    this->ending_height = e_height;
  }
}

void Mode::setMode(int s_height, int s_rotation) {
  this->steps_height = s_height;
  this->steps_rotation = s_rotation;
  this->starting_height = 0;
  this->ending_height = revolution_to_top;
}

void Mode::run_mode() {

  int steps_per_height;
  int steps_per_rotation;

  if (steps_height < 1 || steps_height > 50 || steps_rotation > 200 || steps_rotation < 1) {
    return -1;
  }
  
  
  while (digitalRead(limit_switch) == 1) {
    Stepper1.move(-50);
    Stepper1.runToPosition();
  }
  
  
  delay(800);
  
  Stepper1.move(starting_height * steps_Revolution);
  Stepper1.runToPosition();
  
  steps_per_height = ((ending_height - starting_height) * steps_Revolution) / (steps_height - 1);
  steps_per_rotation = (revolution_to_360 * steps_Revolution) / steps_rotation;

  
  for (int i = 0; i < steps_height; i++) {
    if (i != 0) {
      delay(200);
      Stepper1.move(steps_per_height);
      Stepper1.runToPosition();
    }
      
    for (int y = 0; y < steps_rotation;y++) {
      Stepper2.move(steps_per_rotation);
      Stepper2.runToPosition();
      delay(100);
      digitalWrite(shutter,HIGH);
      delay(1000);
      digitalWrite(shutter,LOW);
    }
    

    
  }
  return;
}

int Mode::get_current_rotation() {
  return this->current_rotation;
}
int Mode::get_current_hight() {
  return this->current_hight;
}
int Mode::get_starting_height() {
  return this->starting_height;
}
int Mode::get_ending_height() {
  return this->ending_height;
}
String Mode::get_Name() {
  return this->Name;
}
