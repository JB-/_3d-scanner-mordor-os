#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "Mode.h"


#define rev_max_hight 100
#define joy_stick_button 5
#define led_gruen 2
#define led_rot 3

LiquidCrystal_I2C lcd(0x27, 16, 2);

AccelStepper Stepper_Rotation(4, 10,11,12,13);
AccelStepper Stepper_Height(4, 6, 7, 8, 9);



void setup() {
  
  pinMode(limit_switch, INPUT_PULLUP);
  pinMode(limit_switch_rotation, INPUT_PULLUP);
  pinMode(joy_stick_button, INPUT_PULLUP);
  pinMode(shutter, OUTPUT);
  pinMode(led_rot, OUTPUT);
  pinMode(led_gruen, OUTPUT);

  pinMode(led_rot,LOW);
  pinMode(led_gruen,HIGH);

  lcd.init();
  lcd.backlight();

  Stepper_Height.setMaxSpeed(400.0);
  Stepper_Rotation.setMaxSpeed(260.0);

  Stepper_Height.setAcceleration(2000.0);
  Stepper_Rotation.setAcceleration(1000.0);

  //Bootup();
  
  Stepper_Rotation.move(50);
  Stepper_Rotation.runToPosition();
  Stepper_Height.move(50);
  Stepper_Height.runToPosition();
  Stepper_Rotation.move(-50);
  Stepper_Rotation.runToPosition();
  Stepper_Height.move(-50);
  Stepper_Height.runToPosition();

}

void loop() {
  
  Mode my_modes[6]; 
  my_modes[0] = Mode("HFG Std. Mode", 10, 36 , 1, 40, Stepper_Height, Stepper_Rotation);
  my_modes[1] = Mode("Mode 2", 5, 5 , 4, 40, Stepper_Height, Stepper_Rotation);
  my_modes[2] = Mode("Mode 3", 5, 1 , 4, 30, Stepper_Height, Stepper_Rotation);
  my_modes[3] = Mode("Const height", 2, 2 , 2, 40, Stepper_Height, Stepper_Rotation);
  my_modes[4] = Mode("Photo mode", 1, 40 , 4, 30, Stepper_Height, Stepper_Rotation);
  my_modes[5] = Mode("Toller Modus", 3, 10 , 4, 20, Stepper_Height, Stepper_Rotation);
  
  int index = 0;
  int index_old =1;

  while (true) {
    
    lcd.setCursor(6, 0);
    lcd.print("MENU");

    if (analogRead(1) > 900) {

      lcd.setCursor(14, 1);
      lcd.print("->");

      index++;
      if (index > (sizeof(my_modes)/sizeof(my_modes[0]))-1) {
        index = 0;
      }
    } else if (analogRead(1) < 20) {

      lcd.setCursor(0, 1);
      lcd.print("<-");

      index--;
      if (index < 0) {
        index = (sizeof(my_modes)/sizeof(my_modes[0]))-1;
      }
    }

    delay(100);

    if (index != index_old) {

      lcd.setCursor(0, 1);
      lcd.print("                ");
      lcd.setCursor((16-my_modes[index].get_Name().length())/2, 1);
      lcd.print(my_modes[index].get_Name());
      
      index_old = index;

      lcd.setCursor(0, 1);
      lcd.print("<");
      lcd.setCursor(15, 1);
      lcd.print(">");

      delay(400);
    }

    if (1 != digitalRead(joy_stick_button)) {
      lcd.setCursor(0, 0);
      lcd.print("Program Running:");
      lcd.setCursor(0,1);
      lcd.print(my_modes[index].get_Name());
      lcd.print("       ");
          
      digitalWrite(led_rot,HIGH);
      digitalWrite(led_gruen,LOW);
      
      my_modes[index].run_mode();
      lcd.setCursor(0, 0);
      lcd.print("                   ");
      lcd.setCursor(6, 0);
      lcd.print("MENU");
    
      digitalWrite(led_rot,LOW);
      digitalWrite(led_gruen,HIGH);
    }

  }
}

void Bootup() {
  lcd.setCursor(3, 0);
  lcd.print("Welcome to");
  lcd.setCursor(1, 1);
  lcd.print("MORDOR OS 3.01");

  delay(800);

  lcd.setCursor(0, 0);
  for (int i = 0; i < 16; i++) {
    lcd.setCursor(i + 2, 0);
    lcd.print(">");
    delay(25);
    lcd.setCursor(i, 0);
    lcd.print("  ");
    delay(25);
  }

  lcd.setCursor(0, 1);
  for (int i = -1; i < 16; i++) {
    lcd.setCursor(i + 2, 1);
    lcd.print(">");
    delay(25);
    lcd.setCursor(i, 1);
    lcd.print("  ");
    delay(25);
  }

  lcd.setCursor(0, 0);
  lcd.print("                ");
  lcd.setCursor(0, 1);
  lcd.print("                ");
}
